package com.example.randomizer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.*;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnRoll;
    TextView howMany;
    TextView results;
    SeekBar bar;
    Random rand = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRoll =(Button) findViewById(R.id.rollButton);
        howMany =(TextView) findViewById(R.id.howManyTextView);
        results =(TextView) findViewById(R.id.resultsTextView);
        bar = (SeekBar) findViewById(R.id.seekBarId);
        btnRoll.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.rollButton){
            results.setText(String.valueOf(rand.nextInt(bar.getProgress())));
        }
    }
}
